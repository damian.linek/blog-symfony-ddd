<?php

namespace App\Build\Fixture;

use App\Build\Fixture\Helper\FixtureStorage;
use App\Core\Blog\Authors\Domain\Model\Author;
use App\Core\Blog\Categories\Domain\Model\Category;
use App\Core\Blog\Posts\Domain\Factory\PostFactory;
use App\Core\Blog\Posts\Domain\Repository\PostRepositoryInterface;
use App\Core\Blog\Shared\Domain\Model\UuidList;
use App\Core\Blog\Tags\Domain\Model\Tag;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PostFixtures extends AbstractFixtures implements DependentFixtureInterface
{
    public function __construct(
        protected FixtureStorage $storage,
        private readonly PostFactory $factory,
        private readonly PostRepositoryInterface $repository
    ) {
        parent::__construct($this->storage);
    }

    public function load(ObjectManager $manager): void
    {
        $counter = $this->generator->numberBetween(40, 70);

        while ($counter) {
            $this->loadObject();
            --$counter;
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return [self::DEFAULT_GROUP];
    }

    private function loadObject(): void
    {
        $author = $this->generator->randomElement($this->storage->get('authors'));
        \assert($author instanceof Author);

        $category = $this->generator->randomElement($this->storage->get('categories'));
        \assert($category instanceof Category);

        /**
         * @var array<Tag> $tags
         */
        $tags = $this->generator->randomElements(
            $this->storage->get('tags'),
            $this->generator->numberBetween(1, 3)
        );

        $object = $this->factory->create(
            $this->generator->sentence(3),
            $this->generator->sentence(10),
            $this->generator->paragraph($this->generator->numberBetween(20, 30)),
            $author->getUuid(),
            $category->getUuid(),
            new UuidList(array_map(fn (Tag $tag) => $tag->getUuid(), $tags))
        );

        $this->repository->create($object);
        $this->storage->set('posts', $object);
    }

    public function getDependencies(): array
    {
        return [
            AuthorFixtures::class,
            CategoryFixtures::class,
            TagFixtures::class,
        ];
    }
}

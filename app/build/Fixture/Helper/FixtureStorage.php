<?php

namespace App\Build\Fixture\Helper;

/**
 * @template-implements \IteratorAggregate<string, array<object>>
 */
class FixtureStorage implements \IteratorAggregate, \Countable
{
    /**
     * @var array<string, array<object>>
     */
    private array $storage = [];

    /**
     * @return ?array<object>
     */
    public function get(string $key): ?array
    {
        return $this->storage[$key] ?? null;
    }

    public function set(string $key, object $value): void
    {
        if ($this->has($key)) {
            $this->storage[$key][] = $value;

            return;
        }

        $this->storage[$key] = [$value];
    }

    public function has(string $key): bool
    {
        return \array_key_exists($key, $this->storage);
    }

    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->storage);
    }

    public function count(): int
    {
        return \count($this->storage);
    }
}

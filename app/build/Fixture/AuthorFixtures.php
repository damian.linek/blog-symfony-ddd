<?php

namespace App\Build\Fixture;

use App\Build\Fixture\Helper\FixtureStorage;
use App\Core\Blog\Authors\Domain\Factory\AuthorFactory;
use App\Core\Blog\Authors\Domain\Repository\AuthorRepositoryInterface;
use Doctrine\Persistence\ObjectManager;

class AuthorFixtures extends AbstractFixtures
{
    public function __construct(
        protected FixtureStorage $storage,
        private readonly AuthorFactory $factory,
        private readonly AuthorRepositoryInterface $repository
    ) {
        parent::__construct($this->storage);
    }

    public function load(ObjectManager $manager): void
    {
        $counter = $this->generator->numberBetween(10, 20);

        while ($counter) {
            $this->loadObject();
            --$counter;
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return [self::DEFAULT_GROUP];
    }

    private function loadObject(): void
    {
        $object = $this->factory->create(
            $this->generator->firstName,
            $this->generator->lastName,
            $this->generator->realText()
        );

        $this->repository->create($object);
        $this->storage->set('authors', $object);
    }
}

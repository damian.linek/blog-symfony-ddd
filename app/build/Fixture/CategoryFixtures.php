<?php

namespace App\Build\Fixture;

use App\Build\Fixture\Helper\FixtureStorage;
use App\Core\Blog\Categories\Domain\Factory\CategoryFactory;
use App\Core\Blog\Categories\Domain\Repository\CategoryRepositoryInterface;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends AbstractFixtures
{
    public function __construct(
        protected FixtureStorage $storage,
        private readonly CategoryFactory $factory,
        private readonly CategoryRepositoryInterface $repository
    ) {
        parent::__construct($this->storage);
    }

    public function load(ObjectManager $manager): void
    {
        $counter = $this->generator->numberBetween(20, 40);

        while ($counter) {
            $this->loadObject();
            --$counter;
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return [self::DEFAULT_GROUP];
    }

    private function loadObject(): void
    {
        $object = $this->factory->create(
            $this->generator->sentence(3),
            $this->generator->paragraph(10)
        );

        $this->repository->create($object);
        $this->storage->set('categories', $object);
    }
}

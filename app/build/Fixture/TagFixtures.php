<?php

namespace App\Build\Fixture;

use App\Build\Fixture\Helper\FixtureStorage;
use App\Core\Blog\Tags\Domain\Factory\TagFactory;
use App\Core\Blog\Tags\Domain\Repository\TagRepositoryInterface;
use Doctrine\Persistence\ObjectManager;

class TagFixtures extends AbstractFixtures
{
    public function __construct(
        protected FixtureStorage $storage,
        private readonly TagFactory $factory,
        private readonly TagRepositoryInterface $repository
    ) {
        parent::__construct($this->storage);
    }

    public function load(ObjectManager $manager): void
    {
        $counter = $this->generator->numberBetween(20, 30);

        while ($counter) {
            $this->loadObject();
            --$counter;
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return [self::DEFAULT_GROUP];
    }

    private function loadObject(): void
    {
        $object = $this->factory->create(
            $this->generator->unique()->word,
        );

        $this->repository->create($object);
        $this->storage->set('tags', $object);
    }
}

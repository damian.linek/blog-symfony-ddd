<?php

namespace App\Build\Fixture;

use App\Build\Fixture\Helper\FixtureStorage;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

abstract class AbstractFixtures extends Fixture implements FixtureGroupInterface
{
    protected const DEFAULT_GROUP = 'default';

    protected Generator $generator;

    public function __construct(
        protected FixtureStorage $storage
    ) {
        $this->generator = Factory::create('pl_PL');
    }

    abstract public function load(ObjectManager $manager): void;

    abstract public static function getGroups(): array;
}

<?php

namespace App\Shared\Infrastructure\Doctrine\DBAL\Types;

use App\Shared\Domain\Model\AbstractUuidList;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Exception\InvalidType;
use Doctrine\DBAL\Types\Exception\SerializationFailed;
use Doctrine\DBAL\Types\Type;

abstract class AbstractUuidListType extends Type
{
    private string $name = 'uuid_list';

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param array<string> $uuids
     */
    abstract protected function getUuidListClass(array $uuids): AbstractUuidList;

    /**
     * {@inheritDoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if (null === $value) {
            return null;
        }

        if (!$value instanceof AbstractUuidList) {
            throw InvalidType::new($value, $this->getName(), ['null', AbstractUuidList::class]);
        }

        $encodedValue = json_encode($value);
        if (!\is_string($encodedValue)) {
            throw SerializationFailed::new($value, 'json', json_last_error_msg());
        }

        return $encodedValue;
    }

    /**
     * {@inheritDoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): ?AbstractUuidList
    {
        if (null === $value) {
            return null;
        }

        if (!\is_string($value)) {
            throw InvalidType::new($value, $this->getName(), ['null', 'string']);
        }

        if (!json_validate($value)) {
            throw SerializationFailed::new($value, 'json', json_last_error_msg());
        }

        $decodedValue = json_decode($value);
        \assert(\is_array($decodedValue));

        return $this->getUuidListClass($decodedValue);
    }

    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return $platform->getClobTypeDeclarationSQL($column);
    }
}

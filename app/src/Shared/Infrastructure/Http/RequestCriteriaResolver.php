<?php

namespace App\Shared\Infrastructure\Http;

use App\Shared\Application\Criteria\CriteriaFactoryInterface;
use App\Shared\Application\Criteria\CriteriaInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class RequestCriteriaResolver implements ValueResolverInterface
{
    /**
     * @param iterable<CriteriaFactoryInterface> $factories
     */
    public function __construct(
        private iterable $factories
    ) {
    }

    /**
     * @return iterable<mixed>
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $type = $argument->getType();
        if (null !== $type && is_subclass_of($type, CriteriaInterface::class)) {
            $data = $this->parseQueryString($request);
            foreach ($this->factories as $factory) {
                if ($factory->supports($type)) {
                    return [$factory->create($data)];
                }
            }
        }

        return [];
    }

    /**
     * @return array<string, mixed>
     */
    private function parseQueryString(Request $request): array
    {
        $query = $request->query->all();
        $query = array_map(static fn ($value) => (array) (new JsonDecode())->decode($value, JsonEncoder::FORMAT, [JsonDecode::ASSOCIATIVE => true]), $query);

        $attributes = $request->attributes->all();
        foreach ($attributes as $key => $attribute) {
            if (str_starts_with($key, '_')) {
                continue;
            }

            \assert(\is_string($key));
            if (!isset($query[$key])) {
                $query[$key] = $attribute;
            }
        }

        return $query;
    }
}

<?php

namespace App\Shared\Infrastructure\Http;

use App\Shared\Domain\Data\DataDtoInterface;
use App\Shared\Domain\Data\DataFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class RequestDataResolver implements ValueResolverInterface
{
    /**
     * @param iterable<DataFactoryInterface> $factories
     */
    public function __construct(
        private iterable $factories
    ) {
    }

    /**
     * @return iterable<mixed>
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $type = $argument->getType();
        if (null !== $type && is_subclass_of($type, DataDtoInterface::class)) {
            $data = $this->parseRequest($request);
            foreach ($this->factories as $factory) {
                if ($factory->supports($type)) {
                    return [$factory->create($data)];
                }
            }
        }

        return [];
    }

    /**
     * @return array<mixed>
     */
    private function parseRequest(Request $request): array
    {
        if ('json' === $request->getContentTypeFormat()) {
            $json = (new JsonDecode())->decode($request->getContent(), JsonEncoder::FORMAT, [JsonDecode::ASSOCIATIVE => true]);
            \assert(\is_array($json));

            return $json;
        }

        return $request->request->all();
    }
}

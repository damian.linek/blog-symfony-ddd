<?php

namespace App\Shared\Infrastructure\Uuid;

use App\Shared\Domain\Uuid\UuidGeneratorInterface;
use Symfony\Component\Uid\UuidV7;

class UuidGenerator implements UuidGeneratorInterface
{
    public function generate(): string
    {
        return (string) UuidV7::v7();
    }
}

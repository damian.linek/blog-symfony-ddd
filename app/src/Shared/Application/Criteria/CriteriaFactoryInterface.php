<?php

namespace App\Shared\Application\Criteria;

interface CriteriaFactoryInterface
{
    /**
     * @param array<string, mixed> $data
     */
    public function create(array $data): CriteriaInterface;

    /**
     * @param class-string $class
     */
    public function supports(string $class): bool;
}

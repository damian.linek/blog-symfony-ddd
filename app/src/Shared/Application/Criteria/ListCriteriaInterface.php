<?php

namespace App\Shared\Application\Criteria;

interface ListCriteriaInterface extends CriteriaInterface
{
    /**
     * @return array<string, mixed>
     */
    public function getCriteria(): array;

    /**
     * @return array<string, string>
     */
    public function getOrderBy(): array;

    public function getLimit(): int;

    public function getOffset(): int;
}

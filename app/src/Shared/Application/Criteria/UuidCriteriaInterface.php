<?php

namespace App\Shared\Application\Criteria;

use App\Shared\Domain\Model\AbstractUuid;

interface UuidCriteriaInterface extends CriteriaInterface
{
    public function getUuid(): AbstractUuid;
}

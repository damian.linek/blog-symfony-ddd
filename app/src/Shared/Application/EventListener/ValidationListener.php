<?php

namespace App\Shared\Application\EventListener;

use App\Shared\Application\Exception\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ValidationListener
{
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        if (!$exception instanceof ValidationException) {
            return;
        }

        $event->setResponse(
            new JsonResponse(
                ['errors' => $exception->getErrors()],
                Response::HTTP_BAD_REQUEST
            )
        );
    }
}

<?php

declare(strict_types=1);

namespace App\Shared\Application\Exception;

class ValidationException extends \RuntimeException
{
    public function __construct(
        private readonly ConstraintViolationList $errors
    ) {
        parent::__construct();
    }

    public function getErrors(): ConstraintViolationList
    {
        return $this->errors;
    }
}

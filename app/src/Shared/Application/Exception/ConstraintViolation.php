<?php

namespace App\Shared\Application\Exception;

class ConstraintViolation
{
    public function __construct(
        public readonly string $property,
        public readonly string $message
    ) {
    }
}

<?php

namespace App\Shared\Application\Exception;

class ConstraintViolationList implements \JsonSerializable
{
    /**
     * @param array<ConstraintViolation> $errors
     */
    public function __construct(
        private readonly array $errors
    ) {
    }

    public function jsonSerialize(): mixed
    {
        return $this->errors;
    }
}

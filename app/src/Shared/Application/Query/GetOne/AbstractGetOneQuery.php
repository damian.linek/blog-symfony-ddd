<?php

namespace App\Shared\Application\Query\GetOne;

use App\Shared\Application\Criteria\UuidCriteriaInterface;
use App\Shared\Application\Query\QueryInterface;
use App\Shared\Domain\Model\AbstractUuid;

abstract class AbstractGetOneQuery implements QueryInterface
{
    public function __construct(
        protected readonly UuidCriteriaInterface $criteria
    ) {
    }

    public function getUuid(): AbstractUuid
    {
        return $this->criteria->getUuid();
    }
}

<?php

namespace App\Shared\Application\Query\GetOne;

use App\Shared\Application\Query\GetList\AbstractGetListQuery;

trait GetOne
{
    protected function getOne(AbstractGetListQuery $query): GetOneDtoInterface
    {
        $object = $this->repository->findOneBy(
            $query->getCriteria(),
            $query->getOrderBy(),
            $query->getLimit(),
            $query->getOffset()
        );

        return $this->factory->create($object);
    }
}

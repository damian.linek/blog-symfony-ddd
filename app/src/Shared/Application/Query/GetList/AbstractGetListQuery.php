<?php

namespace App\Shared\Application\Query\GetList;

use App\Shared\Application\Criteria\ListCriteriaInterface;
use App\Shared\Application\Query\QueryInterface;

abstract class AbstractGetListQuery implements QueryInterface
{
    public function __construct(
        protected readonly ListCriteriaInterface $criteria
    ) {
    }

    /**
     * @return array<string, mixed>
     */
    public function getCriteria(): array
    {
        return $this->criteria->getCriteria();
    }

    /**
     * @return array<string>
     */
    public function getOrderBy(): array
    {
        return $this->criteria->getOrderBy();
    }

    public function getLimit(): int
    {
        return $this->criteria->getLimit();
    }

    public function getOffset(): int
    {
        return $this->criteria->getOffset();
    }
}

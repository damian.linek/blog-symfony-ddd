<?php

namespace App\Shared\Application\Query\GetList;

trait GetList
{
    protected function getList(AbstractGetListQuery $query): GetListDtoInterface
    {
        $objects = $this->repository->findBy(
            $query->getCriteria(),
            $query->getOrderBy(),
            $query->getLimit(),
            $query->getOffset()
        );

        $total = $this->repository->count($query->getCriteria());

        return $this->factory->create($objects, $total);
    }
}

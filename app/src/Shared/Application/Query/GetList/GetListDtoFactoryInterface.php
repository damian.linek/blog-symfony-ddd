<?php

namespace App\Shared\Application\Query\GetList;

interface GetListDtoFactoryInterface
{
    /**
     * @param array<object> $objects
     */
    public function create(
        array $objects,
        int $total
    ): GetListDtoInterface;
}

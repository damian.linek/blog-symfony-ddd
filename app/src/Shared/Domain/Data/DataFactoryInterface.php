<?php

namespace App\Shared\Domain\Data;

interface DataFactoryInterface
{
    /**
     * @param array<string, mixed> $data
     */
    public function create(array $data): AbstractData;

    /**
     * @param class-string $class
     */
    public function supports(string $class): bool;
}

<?php

namespace App\Shared\Domain\Model;

abstract class AbstractUuidList implements \JsonSerializable
{
    /**
     * @param array<AbstractUuid> $uuids
     */
    public function __construct(
        private readonly array $uuids
    ) {
    }

    /**
     * @return array<AbstractUuid>
     */
    public function getUuids(): array
    {
        return $this->uuids;
    }

    /**
     * @return array<string>
     */
    public function jsonSerialize(): array
    {
        return array_map(fn (AbstractUuid $uuid) => $uuid->uuid(), $this->getUuids());
    }
}

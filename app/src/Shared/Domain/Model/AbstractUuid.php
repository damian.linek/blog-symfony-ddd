<?php

namespace App\Shared\Domain\Model;

abstract class AbstractUuid implements \Stringable
{
    public function __construct(
        private readonly string $uuid,
    ) {
    }

    public function uuid(): string
    {
        return $this->uuid;
    }

    public function __toString(): string
    {
        return $this->uuid();
    }
}

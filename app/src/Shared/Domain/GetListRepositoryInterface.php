<?php

namespace App\Shared\Domain;

interface GetListRepositoryInterface
{
    /**
     * @param array<string, mixed>       $criteria
     * @param array<string, string>|null $orderBy
     *
     * @return array<object>
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null);

    /**
     * @param array<string, mixed> $criteria
     *
     * @return int
     */
    public function count(array $criteria);
}

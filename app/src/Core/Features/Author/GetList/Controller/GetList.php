<?php

declare(strict_types=1);

namespace App\Core\Features\Author\GetList\Controller;

use App\Core\Blog\Authors\Application\Criteria\ListCriteria;
use App\Core\Blog\Authors\Application\GetList\GetListDto;
use App\Core\Blog\Authors\Application\GetList\Query\GetListQuery;
use App\Core\Features\Author\GetList\GetListResponse;
use App\Shared\Application\Query\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class GetList extends AbstractController
{
    public function __construct(
        private readonly QueryBusInterface $queryBus
    ) {
    }

    #[Route('/author/', name: 'author_list', methods: ['GET'])]
    public function __invoke(ListCriteria $criteria): JsonResponse
    {
        $authors = $this->queryBus->handle(
            new GetListQuery($criteria)
        );

        if (!$authors instanceof GetListDto) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse(GetListResponse::fromListDto($authors));
    }
}

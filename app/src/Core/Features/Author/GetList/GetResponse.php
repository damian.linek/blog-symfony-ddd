<?php

declare(strict_types=1);

namespace App\Core\Features\Author\GetList;

use App\Core\Blog\Authors\Application\GetList\GetDto;

final class GetResponse
{
    private function __construct(
        public readonly string $id,
        public readonly string $firstname,
        public readonly string $lastname,
    ) {
    }

    public static function fromDto(GetDto $dto): self
    {
        return new self(
            $dto->uuid,
            $dto->firstname,
            $dto->lastname,
        );
    }
}

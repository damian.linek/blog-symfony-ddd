<?php

declare(strict_types=1);

namespace App\Core\Features\Author\GetOne\Controller;

use App\Core\Blog\Authors\Application\GetOne\GetDto;
use App\Core\Blog\Authors\Application\GetOne\Query\GetOneQuery;
use App\Core\Blog\Shared\Application\Criteria\UuidCriteria;
use App\Core\Features\Author\GetOne\GetResponse;
use App\Shared\Application\Query\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class GetOne extends AbstractController
{
    public function __construct(
        private readonly QueryBusInterface $queryBus
    ) {
    }

    #[Route('/author/{uuid}/', name: 'author_show', methods: ['GET'])]
    public function __invoke(UuidCriteria $criteria): JsonResponse
    {
        $author = $this->queryBus->handle(new GetOneQuery($criteria));

        if (!$author instanceof GetDto) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse(GetResponse::fromDto($author));
    }
}

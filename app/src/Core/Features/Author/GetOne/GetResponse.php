<?php

declare(strict_types=1);

namespace App\Core\Features\Author\GetOne;

use App\Core\Blog\Authors\Application\GetOne\GetDto;

final class GetResponse
{
    private function __construct(
        public readonly string $id,
        public readonly string $firstname,
        public readonly string $lastname,
        public readonly string $shortDesc,
        public readonly string $createdAt,
        public readonly string $updatedAt,
    ) {
    }

    public static function fromDto(GetDto $dto): self
    {
        return new self(
            $dto->uuid,
            $dto->firstname,
            $dto->lastname,
            $dto->shortDesc,
            $dto->createdAt->format('c'),
            $dto->updatedAt->format('c')
        );
    }
}

<?php

declare(strict_types=1);

namespace App\Core\Features\Tag\GetList\Controller;

use App\Core\Blog\Tags\Application\Criteria\ListCriteria;
use App\Core\Blog\Tags\Application\GetList\GetListDto;
use App\Core\Blog\Tags\Application\GetList\Query\GetListQuery;
use App\Core\Features\Tag\GetList\GetListResponse;
use App\Shared\Application\Query\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class GetList extends AbstractController
{
    public function __construct(
        private readonly QueryBusInterface $queryBus
    ) {
    }

    #[Route('/tag/', name: 'tag_list', methods: ['GET'])]
    public function __invoke(ListCriteria $criteria): JsonResponse
    {
        $tags = $this->queryBus->handle(
            new GetListQuery($criteria)
        );

        if (!$tags instanceof GetListDto) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse(GetListResponse::fromListDto($tags));
    }
}

<?php

declare(strict_types=1);

namespace App\Core\Features\Tag\GetList;

use App\Core\Blog\Tags\Application\GetList\GetDto;

final class GetResponse
{
    private function __construct(
        public readonly string $id,
        public readonly string $title,
    ) {
    }

    public static function fromDto(GetDto $dto): self
    {
        return new self(
            $dto->uuid,
            $dto->title,
        );
    }
}

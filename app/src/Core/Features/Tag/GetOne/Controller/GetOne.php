<?php

declare(strict_types=1);

namespace App\Core\Features\Tag\GetOne\Controller;

use App\Core\Blog\Shared\Application\Criteria\UuidCriteria;
use App\Core\Blog\Tags\Application\GetOne\GetDto;
use App\Core\Blog\Tags\Application\GetOne\Query\GetOneQuery;
use App\Core\Features\Tag\GetOne\GetResponse;
use App\Shared\Application\Query\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class GetOne extends AbstractController
{
    public function __construct(
        private readonly QueryBusInterface $queryBus
    ) {
    }

    #[Route('/tag/{uuid}/', name: 'tag_show', methods: ['GET'])]
    public function __invoke(UuidCriteria $criteria): JsonResponse
    {
        $tag = $this->queryBus->handle(new GetOneQuery($criteria->getUuid()));

        if (!$tag instanceof GetDto) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse(GetResponse::fromDto($tag));
    }
}

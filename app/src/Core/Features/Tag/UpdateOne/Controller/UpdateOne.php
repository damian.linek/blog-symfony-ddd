<?php

declare(strict_types=1);

namespace App\Core\Features\Tag\UpdateOne\Controller;

use App\Core\Blog\Shared\Application\Criteria\UuidCriteria;
use App\Core\Blog\Tags\Application\Data\UpdateOneDto;
use App\Core\Blog\Tags\Application\GetOne\GetDto;
use App\Core\Blog\Tags\Application\GetOne\Query\GetOneQuery;
use App\Core\Blog\Tags\Application\UpdateOne\Command\UpdateOneCommand;
use App\Core\Features\Tag\UpdateOne\UpdateResponse;
use App\Shared\Application\Command\CommandBusInterface;
use App\Shared\Application\Query\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class UpdateOne extends AbstractController
{
    public function __construct(
        private readonly CommandBusInterface $commandBus,
        private readonly QueryBusInterface $queryBus
    ) {
    }

    #[Route('/tag/{uuid}/', name: 'tag_update', methods: ['PUT'])]
    public function __invoke(UuidCriteria $criteria, UpdateOneDto $dto): JsonResponse
    {
        $this->commandBus->dispatch(new UpdateOneCommand($criteria, $dto));
        $author = $this->queryBus->handle(new GetOneQuery($criteria->getUuid()));

        if (!$author instanceof GetDto) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse(UpdateResponse::fromDto($author));
    }
}

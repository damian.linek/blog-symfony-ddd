<?php

declare(strict_types=1);

namespace App\Core\Features\Post\GetList;

use App\Core\Blog\Posts\Application\GetList\GetDto;

final class GetResponse
{
    private function __construct(
        public readonly string $id,
        public readonly string $title,
        public readonly string $shortDesc,
        public readonly string $authorUuid,
        public readonly string $categoryUuid,
    ) {
    }

    public static function fromDto(GetDto $dto): self
    {
        return new self(
            $dto->uuid,
            $dto->title,
            $dto->shortDesc,
            $dto->authorUuid,
            $dto->categoryUuid,
        );
    }
}

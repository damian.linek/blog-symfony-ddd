<?php

declare(strict_types=1);

namespace App\Core\Features\Post\GetList;

use App\Core\Blog\Posts\Application\GetList\GetDto;
use App\Core\Blog\Posts\Application\GetList\GetListDto;

final class GetListResponse
{
    /**
     * @param array<GetResponse> $data
     */
    private function __construct(
        public readonly array $data,
        public readonly int $total
    ) {
    }

    public static function fromListDto(GetListDto $list): self
    {
        return new self(
            array_map(fn (GetDto $dto) => GetResponse::fromDto($dto), $list->posts),
            $list->total
        );
    }
}

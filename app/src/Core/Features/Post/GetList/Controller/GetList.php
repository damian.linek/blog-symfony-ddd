<?php

declare(strict_types=1);

namespace App\Core\Features\Post\GetList\Controller;

use App\Core\Blog\Posts\Application\Criteria\ListCriteria;
use App\Core\Blog\Posts\Application\GetList\GetListDto;
use App\Core\Blog\Posts\Application\GetList\Query\GetListQuery;
use App\Core\Features\Post\GetList\GetListResponse;
use App\Shared\Application\Query\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class GetList extends AbstractController
{
    public function __construct(
        private readonly QueryBusInterface $queryBus
    ) {
    }

    #[Route('/post/', name: 'post_list', methods: ['GET'])]
    public function __invoke(ListCriteria $criteria): JsonResponse
    {
        $posts = $this->queryBus->handle(
            new GetListQuery($criteria)
        );

        if (!$posts instanceof GetListDto) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse(GetListResponse::fromListDto($posts));
    }
}

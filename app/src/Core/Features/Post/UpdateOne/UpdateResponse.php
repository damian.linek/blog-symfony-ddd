<?php

declare(strict_types=1);

namespace App\Core\Features\Post\UpdateOne;

use App\Core\Blog\Posts\Application\GetOne\GetDto;

final class UpdateResponse
{
    /**
     * @param array<string> $tagUuidList
     */
    private function __construct(
        public readonly string $id,
        public readonly string $title,
        public readonly string $shortDesc,
        public readonly string $description,
        public readonly string $categoryUuid,
        public readonly string $authorUuid,
        public readonly array $tagUuidList,
        public readonly string $createdAt,
        public readonly string $updatedAt,
    ) {
    }

    public static function fromDto(GetDto $dto): self
    {
        return new self(
            $dto->uuid,
            $dto->title,
            $dto->shortDesc,
            $dto->description,
            $dto->categoryUuid,
            $dto->authorUuid,
            $dto->tagUuidList,
            $dto->createdAt->format('c'),
            $dto->updatedAt->format('c')
        );
    }
}

<?php

declare(strict_types=1);

namespace App\Core\Features\Category\GetOne\Controller;

use App\Core\Blog\Categories\Application\GetOne\GetDto;
use App\Core\Blog\Categories\Application\GetOne\Query\GetOneQuery;
use App\Core\Blog\Shared\Application\Criteria\UuidCriteria;
use App\Core\Features\Category\GetOne\GetResponse;
use App\Shared\Application\Query\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class GetOne extends AbstractController
{
    public function __construct(
        private readonly QueryBusInterface $queryBus
    ) {
    }

    #[Route('/category/{uuid}/', name: 'category_show', methods: ['GET'])]
    public function __invoke(UuidCriteria $criteria): JsonResponse
    {
        $category = $this->queryBus->handle(new GetOneQuery($criteria->getUuid()));

        if (!$category instanceof GetDto) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse(GetResponse::fromDto($category));
    }
}

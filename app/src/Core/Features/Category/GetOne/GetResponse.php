<?php

declare(strict_types=1);

namespace App\Core\Features\Category\GetOne;

use App\Core\Blog\Categories\Application\GetOne\GetDto;

final class GetResponse
{
    private function __construct(
        public readonly string $id,
        public readonly string $title,
        public readonly string $description,
        public readonly string $createdAt,
        public readonly string $updatedAt,
    ) {
    }

    public static function fromDto(GetDto $dto): self
    {
        return new self(
            $dto->uuid,
            $dto->title,
            $dto->description,
            $dto->createdAt->format('c'),
            $dto->updatedAt->format('c')
        );
    }
}

<?php

declare(strict_types=1);

namespace App\Core\Features\Category\GetList;

use App\Core\Blog\Categories\Application\GetList\GetDto;

final class GetResponse
{
    private function __construct(
        public readonly string $id,
        public readonly string $title,
    ) {
    }

    public static function fromDto(GetDto $dto): self
    {
        return new self(
            $dto->uuid,
            $dto->title,
        );
    }
}

<?php

namespace App\Core\Blog\Tags\Domain\Repository;

use App\Core\Blog\Tags\Domain\Model\Tag;
use App\Shared\Domain\GetListRepositoryInterface;

interface TagRepositoryInterface extends GetListRepositoryInterface
{
    /**
     * @return ?Tag
     */
    public function find(mixed $id, ?int $lockMode = null, ?int $lockVersion = null);

    /**
     * @param array<string, mixed>       $criteria
     * @param array<string, string>|null $orderBy
     *
     * @return array<Tag>
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null);

    /**
     * @return array<Tag>
     */
    public function findAll();

    /**
     * @param array<string, mixed>       $criteria
     * @param array<string, string>|null $orderBy
     *
     * @return array<Tag>
     */
    public function findOneBy(array $criteria, ?array $orderBy = null);

    /**
     * @param array<string, mixed> $criteria
     *
     * @return int
     */
    public function count(array $criteria);

    public function create(Tag $tag): void;

    public function commit(): void;
}

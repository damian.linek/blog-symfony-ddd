<?php

namespace App\Core\Blog\Tags\Domain\Factory;

use App\Core\Blog\Shared\Domain\Model\Uuid;
use App\Core\Blog\Tags\Domain\Model\Tag;
use App\Shared\Domain\Uuid\UuidGeneratorInterface;

class TagFactory
{
    public function __construct(
        private readonly UuidGeneratorInterface $uuidGenerator
    ) {
    }

    public function create(
        string $title,
    ): Tag {
        return new Tag(
            new Uuid($this->uuidGenerator->generate()),
            $title,
            new \DateTimeImmutable(),
            new \DateTimeImmutable(),
        );
    }

    public function updateOne(
        Tag $tag,
        string $title
    ): Tag {
        return $this->update($tag->setTitle($title));
    }

    private function update(Tag $tag): Tag
    {
        return $tag
            ->setUpdatedAt(new \DateTimeImmutable());
    }
}

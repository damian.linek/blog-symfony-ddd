<?php

namespace App\Core\Blog\Tags\Infrastructure\Repository;

use App\Core\Blog\Tags\Domain\Model\Tag;
use App\Core\Blog\Tags\Domain\Repository\TagRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\LockMode;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tag|null   find(mixed $id, LockMode|int|null $lockMode = null, int|null $lockVersion = null)
 * @method Tag|null   findOneBy(array $criteria, ?array $orderBy = null)
 * @method array<Tag> findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null)
 * @method array<Tag> findAll()
 * @method int        count(array $criteria = [])
 */
class TagRepository extends ServiceEntityRepository implements TagRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tag::class);
    }

    public function create(Tag $tag): void
    {
        $this->getEntityManager()->persist($tag);
        $this->commit();
    }

    public function commit(): void
    {
        $this->getEntityManager()->flush();
    }
}

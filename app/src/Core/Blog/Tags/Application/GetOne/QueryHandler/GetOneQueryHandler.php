<?php

namespace App\Core\Blog\Tags\Application\GetOne\QueryHandler;

use App\Core\Blog\Tags\Application\GetOne\GetDto;
use App\Core\Blog\Tags\Application\GetOne\Query\GetOneQuery;
use App\Core\Blog\Tags\Domain\Model\Tag;
use App\Core\Blog\Tags\Domain\Repository\TagRepositoryInterface;
use App\Shared\Application\Query\QueryHandlerInterface;

class GetOneQueryHandler implements QueryHandlerInterface
{
    public function __construct(
        private readonly TagRepositoryInterface $repository
    ) {
    }

    public function __invoke(GetOneQuery $query): ?GetDto
    {
        $tag = $this->repository->find(
            $query->getUuid()
        );

        if ($tag instanceof Tag) {
            return GetDto::fromEntity($tag);
        }

        return null;
    }
}

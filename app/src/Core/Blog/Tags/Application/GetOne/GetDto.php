<?php

namespace App\Core\Blog\Tags\Application\GetOne;

use App\Core\Blog\Tags\Domain\Model\Tag;

class GetDto
{
    private function __construct(
        public readonly string $uuid,
        public readonly string $title,
        public readonly \DateTimeInterface $createdAt,
        public readonly \DateTimeInterface $updatedAt,
    ) {
    }

    public static function fromEntity(Tag $category): self
    {
        return new self(
            (string) $category->getUuid(),
            $category->getTitle(),
            $category->getCreatedAt(),
            $category->getUpdatedAt(),
        );
    }
}

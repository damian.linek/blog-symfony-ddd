<?php

namespace App\Core\Blog\Tags\Application\GetList;

use App\Shared\Application\Query\GetList\GetListDtoInterface;

final readonly class GetListDto implements GetListDtoInterface
{
    /**
     * @param array<GetDto> $tags
     */
    public function __construct(
        public array $tags,
        public int $total,
    ) {
    }
}

<?php

namespace App\Core\Blog\Tags\Application\GetList;

use App\Core\Blog\Tags\Domain\Model\Tag;

final class GetDto
{
    private function __construct(
        public readonly string $uuid,
        public readonly string $title,
    ) {
    }

    public static function fromEntity(Tag $tag): self
    {
        return new self(
            (string) $tag->getUuid(),
            $tag->getTitle(),
        );
    }
}

<?php

namespace App\Core\Blog\Tags\Application\GetList\Factory;

use App\Core\Blog\Tags\Application\GetList\GetDto;
use App\Core\Blog\Tags\Application\GetList\GetListDto;
use App\Core\Blog\Tags\Domain\Model\Tag;
use App\Shared\Application\Query\GetList\GetListDtoFactoryInterface;
use App\Shared\Application\Query\GetList\GetListDtoInterface;

class GetListDtoFactory implements GetListDtoFactoryInterface
{
    public function create(array $objects, int $total): GetListDtoInterface
    {
        $data = [];
        foreach ($objects as $object) {
            \assert($object instanceof Tag);
            $data[] = GetDto::fromEntity($object);
        }

        return new GetListDto($data, $total);
    }
}

<?php

namespace App\Core\Blog\Tags\Application\Data;

use App\Shared\Domain\Data\AbstractData;

class UpdateOneDto extends AbstractData
{
    private function __construct(
        private readonly string $title,
    ) {
    }

    /**
     * @param array{title: string} $data
     */
    public static function fromArray(array $data): self
    {
        return new self(
            $data['title']
        );
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}

<?php

namespace App\Core\Blog\Tags\Application\Factory;

use App\Core\Blog\Shared\Application\Criteria\FilterBag;
use App\Core\Blog\Shared\Application\Criteria\Pagination;
use App\Core\Blog\Shared\Application\Criteria\Sort;
use App\Core\Blog\Shared\Application\Factory\AbstractListCriteriaFactory;
use App\Core\Blog\Tags\Application\Criteria\ListCriteria;
use App\Core\Blog\Tags\Domain\Model\Tag;
use App\Shared\Application\Criteria\CriteriaFactoryInterface;
use App\Shared\Application\Criteria\CriteriaInterface;

class ListCriteriaFactory extends AbstractListCriteriaFactory implements CriteriaFactoryInterface
{
    /**
     * @param array<string, mixed> $data
     */
    public function create(array $data): CriteriaInterface
    {
        $this->validate($data);
        $refactoredData = $this->refactorData($data);

        return new ListCriteria(
            new FilterBag($refactoredData['filters']),
            new Sort($refactoredData['sort']['field'], $refactoredData['sort']['order']),
            new Pagination($refactoredData['pagination']['page'], $refactoredData['pagination']['limit'])
        );
    }

    /**
     * @param class-string $class
     */
    public function supports(string $class): bool
    {
        return ListCriteria::class === $class;
    }

    protected function getModel(): string
    {
        return Tag::class;
    }
}

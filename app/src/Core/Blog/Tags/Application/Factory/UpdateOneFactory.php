<?php

namespace App\Core\Blog\Tags\Application\Factory;

use App\Core\Blog\Shared\Application\Factory\AbstractDataFactory;
use App\Core\Blog\Shared\Domain\Validator\ValidatorInterface;
use App\Core\Blog\Tags\Application\Data\UpdateOneDto;
use App\Shared\Application\Exception\ConstraintViolation;
use App\Shared\Application\Exception\ConstraintViolationList;
use App\Shared\Application\Exception\ValidationException;

class UpdateOneFactory extends AbstractDataFactory
{
    public function __construct(
        private readonly ValidatorInterface $requiredValidator,
        private readonly ValidatorInterface $stringValidator
    ) {
    }

    /**
     * @param array<string, mixed> $data
     */
    public function create(array $data): UpdateOneDto
    {
        $this->validate($data);

        return UpdateOneDto::fromArray($this->refactorData($data));
    }

    /**
     * @param class-string $class
     */
    public function supports(string $class): bool
    {
        return UpdateOneDto::class === $class;
    }

    /**
     * @param array<mixed> $data
     *
     * @return array{title: string}
     */
    private function refactorData(array $data): array
    {
        \assert(\is_string($data['title']));

        return [
            'title' => $data['title'],
        ];
    }

    /**
     * @param array<mixed> $data
     */
    private function validate(array $data): void
    {
        $errors = [];

        if (!$this->requiredValidator->validate('title', $data)) {
            $errors[] = new ConstraintViolation('title', $this->requiredValidator->getMessage());
        }

        if (!$this->stringValidator->validate('title', $data)) {
            $errors[] = new ConstraintViolation('title', $this->stringValidator->getMessage());
        }

        if (!empty($errors)) {
            throw new ValidationException(new ConstraintViolationList($errors));
        }
    }
}

<?php

namespace App\Core\Blog\Tags\Application\UpdateOne\Command;

use App\Core\Blog\Shared\Application\Criteria\UuidCriteria;
use App\Core\Blog\Shared\Domain\Model\Uuid;
use App\Core\Blog\Tags\Application\Data\UpdateOneDto;
use App\Shared\Application\Command\CommandInterface;

class UpdateOneCommand implements CommandInterface
{
    public function __construct(
        private readonly UuidCriteria $criteria,
        private readonly UpdateOneDto $dto
    ) {
    }

    public function getDto(): UpdateOneDto
    {
        return $this->dto;
    }

    public function getUuid(): Uuid
    {
        return $this->criteria->getUuid();
    }

    public function getTitle(): string
    {
        return $this->getDto()->getTitle();
    }
}

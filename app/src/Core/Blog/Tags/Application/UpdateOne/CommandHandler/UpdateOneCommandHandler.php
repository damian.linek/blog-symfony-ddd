<?php

namespace App\Core\Blog\Tags\Application\UpdateOne\CommandHandler;

use App\Core\Blog\Tags\Application\UpdateOne\Command\UpdateOneCommand;
use App\Core\Blog\Tags\Domain\Factory\TagFactory;
use App\Core\Blog\Tags\Domain\Model\Tag;
use App\Core\Blog\Tags\Domain\Repository\TagRepositoryInterface;
use App\Shared\Application\Command\CommandHandlerInterface;

class UpdateOneCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private readonly TagRepositoryInterface $repository,
        private readonly TagFactory $factory
    ) {
    }

    public function __invoke(UpdateOneCommand $command): void
    {
        $object = $this->repository->find($command->getUuid());

        if ($object instanceof Tag) {
            $this->factory->updateOne($object, $command->getTitle());

            $this->repository->commit();
        }
    }
}

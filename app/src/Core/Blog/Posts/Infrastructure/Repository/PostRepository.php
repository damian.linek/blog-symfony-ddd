<?php

namespace App\Core\Blog\Posts\Infrastructure\Repository;

use App\Core\Blog\Posts\Domain\Model\Post;
use App\Core\Blog\Posts\Domain\Repository\PostRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\LockMode;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Post|null   find(mixed $id, LockMode|int|null $lockMode = null, int|null $lockVersion = null)
 * @method Post|null   findOneBy(array $criteria, ?array $orderBy = null)
 * @method array<Post> findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null)
 * @method array<Post> findAll()
 * @method int         count(array $criteria = [])
 */
class PostRepository extends ServiceEntityRepository implements PostRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    public function create(Post $post): void
    {
        $this->getEntityManager()->persist($post);
        $this->getEntityManager()->flush();
    }

    public function commit(): void
    {
        $this->getEntityManager()->flush();
    }
}

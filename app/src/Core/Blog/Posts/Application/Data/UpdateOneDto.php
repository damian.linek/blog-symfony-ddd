<?php

namespace App\Core\Blog\Posts\Application\Data;

use App\Core\Blog\Shared\Domain\Model\Uuid;
use App\Core\Blog\Shared\Domain\Model\UuidList;
use App\Shared\Domain\Data\AbstractData;

class UpdateOneDto extends AbstractData
{
    private function __construct(
        private readonly string $title,
        private readonly string $shortDesc,
        private readonly string $description,
        private readonly Uuid $categoryUuid,
        private readonly Uuid $authorUuid,
        private readonly UuidList $tagUuidList,
    ) {
    }

    /**
     * @param array{title: string, shortDesc: string, description: string, categoryUuid: string, authorUuid: string, tagUuidList: array<string>} $data
     */
    public static function fromArray(array $data): self
    {
        return new self(
            $data['title'],
            $data['shortDesc'],
            $data['description'],
            new Uuid($data['categoryUuid']),
            new Uuid($data['authorUuid']),
            UuidList::fromArray($data['tagUuidList']),
        );
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getShortDesc(): string
    {
        return $this->shortDesc;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getCategoryUuid(): Uuid
    {
        return $this->categoryUuid;
    }

    public function getAuthorUuid(): Uuid
    {
        return $this->authorUuid;
    }

    public function getTagUuidList(): UuidList
    {
        return $this->tagUuidList;
    }
}

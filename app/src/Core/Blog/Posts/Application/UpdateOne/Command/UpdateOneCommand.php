<?php

namespace App\Core\Blog\Posts\Application\UpdateOne\Command;

use App\Core\Blog\Posts\Application\Data\UpdateOneDto;
use App\Core\Blog\Shared\Application\Criteria\UuidCriteria;
use App\Core\Blog\Shared\Domain\Model\Uuid;
use App\Core\Blog\Shared\Domain\Model\UuidList;
use App\Shared\Application\Command\CommandInterface;

class UpdateOneCommand implements CommandInterface
{
    public function __construct(
        private readonly UuidCriteria $criteria,
        private readonly UpdateOneDto $dto
    ) {
    }

    public function getDto(): UpdateOneDto
    {
        return $this->dto;
    }

    public function getUuid(): Uuid
    {
        return $this->criteria->getUuid();
    }

    public function getTitle(): string
    {
        return $this->getDto()->getTitle();
    }

    public function getShortDesc(): string
    {
        return $this->getDto()->getShortDesc();
    }

    public function getDescription(): string
    {
        return $this->getDto()->getDescription();
    }

    public function getCategoryUuid(): Uuid
    {
        return $this->getDto()->getCategoryUuid();
    }

    public function getAuthorUuid(): Uuid
    {
        return $this->getDto()->getAuthorUuid();
    }

    public function getTagUuidList(): UuidList
    {
        return $this->getDto()->getTagUuidList();
    }
}

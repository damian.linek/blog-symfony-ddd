<?php

namespace App\Core\Blog\Posts\Application\UpdateOne\CommandHandler;

use App\Core\Blog\Posts\Application\UpdateOne\Command\UpdateOneCommand;
use App\Core\Blog\Posts\Domain\Factory\PostFactory;
use App\Core\Blog\Posts\Domain\Model\Post;
use App\Core\Blog\Posts\Domain\Repository\PostRepositoryInterface;
use App\Shared\Application\Command\CommandHandlerInterface;

class UpdateOneCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private readonly PostRepositoryInterface $repository,
        private readonly PostFactory $factory
    ) {
    }

    public function __invoke(UpdateOneCommand $command): void
    {
        $object = $this->repository->find($command->getUuid());

        if ($object instanceof Post) {
            $this->factory->updateOne(
                $object,
                $command->getTitle(),
                $command->getShortDesc(),
                $command->getDescription(),
                $command->getAuthorUuid(),
                $command->getCategoryUuid(),
                $command->getTagUuidList(),
            );

            $this->repository->commit();
        }
    }
}

<?php

namespace App\Core\Blog\Posts\Application\Factory;

use App\Core\Blog\Posts\Application\Data\UpdateOneDto;
use App\Core\Blog\Shared\Application\Factory\AbstractDataFactory;
use App\Core\Blog\Shared\Domain\Validator\ValidatorInterface;
use App\Shared\Application\Exception\ConstraintViolation;
use App\Shared\Application\Exception\ConstraintViolationList;
use App\Shared\Application\Exception\ValidationException;

class UpdateOneFactory extends AbstractDataFactory
{
    public function __construct(
        private readonly ValidatorInterface $requiredValidator,
        private readonly ValidatorInterface $stringValidator,
        private readonly ValidatorInterface $uuidValidator,
        private readonly ValidatorInterface $arrayValidator,
    ) {
    }

    /**
     * @param array<string, mixed> $data
     */
    public function create(array $data): UpdateOneDto
    {
        $this->validate($data);

        return UpdateOneDto::fromArray($this->refactorData($data));
    }

    /**
     * @param class-string $class
     */
    public function supports(string $class): bool
    {
        return UpdateOneDto::class === $class;
    }

    /**
     * @param array<mixed> $data
     *
     * @return array{title: string, shortDesc: string, description: string, categoryUuid: string, authorUuid: string, tagUuidList: array<string>}
     */
    private function refactorData(array $data): array
    {
        \assert(\is_string($data['title']));
        \assert(\is_string($data['shortDesc']));
        \assert(\is_string($data['description']));
        \assert(\is_string($data['categoryUuid']));
        \assert(\is_string($data['authorUuid']));
        \assert(\is_array($data['tagUuidList']));

        return [
            'title' => $data['title'],
            'shortDesc' => $data['shortDesc'],
            'description' => $data['description'],
            'categoryUuid' => $data['categoryUuid'],
            'authorUuid' => $data['authorUuid'],
            'tagUuidList' => $data['tagUuidList'],
        ];
    }

    /**
     * @param array<mixed> $data
     */
    private function validate(array $data): void
    {
        $errors = $this->validateField('title', $data);
        $errors = array_merge($errors, $this->validateField('shortDesc', $data));
        $errors = array_merge($errors, $this->validateField('description', $data));
        $errors = array_merge($errors, $this->validateUuidField('categoryUuid', $data));
        $errors = array_merge($errors, $this->validateUuidField('authorUuid', $data));
        $errors = array_merge($errors, $this->validateListUuidField('tagUuidList', $data));

        if (!empty($errors)) {
            throw new ValidationException(new ConstraintViolationList($errors));
        }
    }

    /**
     * @param array<mixed> $data
     *
     * @return array<ConstraintViolation>
     */
    private function validateField(string $field, array $data): array
    {
        $errors = [];

        if (!$this->requiredValidator->validate($field, $data)) {
            $errors[] = new ConstraintViolation($field, $this->requiredValidator->getMessage());
        }

        if (!$this->stringValidator->validate($field, $data)) {
            $errors[] = new ConstraintViolation($field, $this->stringValidator->getMessage());
        }

        return $errors;
    }

    /**
     * @param array<mixed> $data
     *
     * @return array<ConstraintViolation>
     */
    private function validateUuidField(string $field, array $data): array
    {
        if (!$this->requiredValidator->validate($field, $data)) {
            return [new ConstraintViolation($field, $this->requiredValidator->getMessage())];
        }

        if (!$this->uuidValidator->validate($field, $data)) {
            return [new ConstraintViolation($field, $this->uuidValidator->getMessage())];
        }

        return [];
    }

    /**
     * @param array<mixed> $data
     *
     * @return array<ConstraintViolation>
     */
    private function validateListUuidField(string $field, array $data): array
    {
        if (!$this->requiredValidator->validate($field, $data)) {
            return [new ConstraintViolation($field, $this->requiredValidator->getMessage())];
        }

        if (!$this->arrayValidator->validate($field, $data)) {
            return [new ConstraintViolation($field, $this->arrayValidator->getMessage())];
        }

        $errors = [];
        \assert(\is_array($data[$field]));
        foreach ($data[$field] as $uuid) {
            if (!$this->uuidValidator->validate('uuid', ['uuid' => $uuid])) {
                return [new ConstraintViolation($field, $this->uuidValidator->getMessage())];
            }
        }

        return $errors;
    }
}

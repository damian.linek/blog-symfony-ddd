<?php

namespace App\Core\Blog\Posts\Application\GetList;

use App\Core\Blog\Posts\Domain\Model\Post;

final readonly class GetDto
{
    private function __construct(
        public string $uuid,
        public string $title,
        public string $shortDesc,
        public string $authorUuid,
        public string $categoryUuid,
    ) {
    }

    public static function fromEntity(Post $post): self
    {
        return new self(
            $post->getUuid()->uuid(),
            $post->getTitle(),
            $post->getShortDesc(),
            $post->getAuthorUuid()->uuid(),
            $post->getCategoryUuid()->uuid(),
        );
    }
}

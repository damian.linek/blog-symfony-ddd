<?php

namespace App\Core\Blog\Posts\Application\GetList\QueryHandler;

use App\Core\Blog\Posts\Application\GetList\Factory\GetListDtoFactory;
use App\Core\Blog\Posts\Application\GetList\Query\GetListQuery;
use App\Core\Blog\Posts\Domain\Repository\PostRepositoryInterface;
use App\Shared\Application\Query\GetList\GetList;
use App\Shared\Application\Query\GetList\GetListDtoInterface;
use App\Shared\Application\Query\QueryHandlerInterface;

class GetListQueryHandler implements QueryHandlerInterface
{
    use GetList;

    public function __construct(
        protected readonly PostRepositoryInterface $repository,
        protected readonly GetListDtoFactory $factory
    ) {
    }

    public function __invoke(GetListQuery $query): GetListDtoInterface
    {
        return $this->getList($query);
    }
}

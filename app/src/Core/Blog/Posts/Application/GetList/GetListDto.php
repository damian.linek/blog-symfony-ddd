<?php

namespace App\Core\Blog\Posts\Application\GetList;

use App\Shared\Application\Query\GetList\GetListDtoInterface;

final readonly class GetListDto implements GetListDtoInterface
{
    /**
     * @param array<GetDto> $posts
     */
    public function __construct(
        public array $posts,
        public int $total,
    ) {
    }
}

<?php

namespace App\Core\Blog\Posts\Application\GetList\Factory;

use App\Core\Blog\Posts\Application\GetList\GetDto;
use App\Core\Blog\Posts\Application\GetList\GetListDto;
use App\Core\Blog\Posts\Domain\Model\Post;
use App\Shared\Application\Query\GetList\GetListDtoFactoryInterface;
use App\Shared\Application\Query\GetList\GetListDtoInterface;

class GetListDtoFactory implements GetListDtoFactoryInterface
{
    public function create(array $objects, int $total): GetListDtoInterface
    {
        $data = [];
        foreach ($objects as $object) {
            \assert($object instanceof Post);
            $data[] = GetDto::fromEntity($object);
        }

        return new GetListDto($data, $total);
    }
}

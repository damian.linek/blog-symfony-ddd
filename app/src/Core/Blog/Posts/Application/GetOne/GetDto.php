<?php

namespace App\Core\Blog\Posts\Application\GetOne;

use App\Core\Blog\Posts\Domain\Model\Post;
use App\Shared\Domain\Model\AbstractUuid;

class GetDto
{
    /**
     * @param array<string> $tagUuidList
     */
    private function __construct(
        public readonly string $uuid,
        public readonly string $title,
        public readonly string $shortDesc,
        public readonly string $description,
        public readonly string $categoryUuid,
        public readonly string $authorUuid,
        public readonly array $tagUuidList,
        public readonly \DateTimeInterface $createdAt,
        public readonly \DateTimeInterface $updatedAt,
    ) {
    }

    public static function fromEntity(Post $post): self
    {
        return new self(
            (string) $post->getUuid(),
            $post->getTitle(),
            $post->getShortDesc(),
            $post->getDescription(),
            (string) $post->getCategoryUuid(),
            (string) $post->getAuthorUuid(),
            array_map(fn (AbstractUuid $uuid) => $uuid->uuid(), $post->getTagUuidList()->getUuids()),
            $post->getCreatedAt(),
            $post->getUpdatedAt(),
        );
    }
}

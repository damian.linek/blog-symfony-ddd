<?php

namespace App\Core\Blog\Posts\Application\GetOne\QueryHandler;

use App\Core\Blog\Posts\Application\GetOne\GetDto;
use App\Core\Blog\Posts\Application\GetOne\Query\GetOneQuery;
use App\Core\Blog\Posts\Domain\Model\Post;
use App\Core\Blog\Posts\Domain\Repository\PostRepositoryInterface;
use App\Shared\Application\Query\QueryHandlerInterface;

class GetOneQueryHandler implements QueryHandlerInterface
{
    public function __construct(
        private readonly PostRepositoryInterface $repository
    ) {
    }

    public function __invoke(GetOneQuery $query): ?GetDto
    {
        $post = $this->repository->find(
            $query->getUuid()
        );

        if ($post instanceof Post) {
            return GetDto::fromEntity($post);
        }

        return null;
    }
}

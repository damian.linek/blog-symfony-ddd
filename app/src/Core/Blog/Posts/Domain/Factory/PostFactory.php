<?php

namespace App\Core\Blog\Posts\Domain\Factory;

use App\Core\Blog\Posts\Domain\Model\Post;
use App\Core\Blog\Shared\Domain\Model\Uuid;
use App\Core\Blog\Shared\Domain\Model\UuidList;
use App\Shared\Domain\Uuid\UuidGeneratorInterface;

class PostFactory
{
    public function __construct(
        private readonly UuidGeneratorInterface $uuidGenerator
    ) {
    }

    public function create(
        string $title,
        string $shortDesc,
        string $description,
        Uuid $author,
        Uuid $category,
        UuidList $tags
    ): Post {
        return new Post(
            new Uuid($this->uuidGenerator->generate()),
            $title,
            $shortDesc,
            $description,
            $category,
            $author,
            $tags,
            new \DateTimeImmutable(),
            new \DateTimeImmutable(),
        );
    }

    public function updateOne(
        Post $post,
        string $title,
        string $shortDesc,
        string $description,
        Uuid $author,
        Uuid $category,
        UuidList $tags
    ): Post {
        return $this->update(
            $post
                ->setTitle($title)
                ->setShortDesc($shortDesc)
                ->setDescription($description)
                ->setAuthorUuid($author)
                ->setCategoryUuid($category)
                ->setTagUuidList($tags)
        );
    }

    private function update(Post $post): Post
    {
        return $post
            ->setUpdatedAt(new \DateTimeImmutable());
    }
}

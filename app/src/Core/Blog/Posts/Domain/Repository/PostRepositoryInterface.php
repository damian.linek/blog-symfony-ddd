<?php

namespace App\Core\Blog\Posts\Domain\Repository;

use App\Core\Blog\Posts\Domain\Model\Post;
use App\Shared\Domain\GetListRepositoryInterface;

interface PostRepositoryInterface extends GetListRepositoryInterface
{
    /**
     * @return ?Post
     */
    public function find(mixed $id, ?int $lockMode = null, ?int $lockVersion = null);

    /**
     * @param array<string, mixed>       $criteria
     * @param array<string, string>|null $orderBy
     *
     * @return array<Post>
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null);

    /**
     * @return array<Post>
     */
    public function findAll();

    /**
     * @param array<string, mixed>       $criteria
     * @param array<string, string>|null $orderBy
     *
     * @return array<Post>
     */
    public function findOneBy(array $criteria, ?array $orderBy = null);

    /**
     * @param array<string, mixed> $criteria
     *
     * @return int
     */
    public function count(array $criteria);

    public function create(Post $post): void;

    public function commit(): void;
}

<?php

namespace App\Core\Blog\Posts\Domain\Model;

use App\Core\Blog\Shared\Domain\Model\Uuid;
use App\Core\Blog\Shared\Domain\Model\UuidList;

class Post
{
    public function __construct(
        private readonly Uuid $uuid,
        private string $title,
        private string $shortDesc,
        private string $description,
        private Uuid $categoryUuid,
        private Uuid $authorUuid,
        private UuidList $tagUuidList,
        private readonly \DateTimeInterface $createdAt,
        private \DateTimeInterface $updatedAt,
    ) {
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getShortDesc(): string
    {
        return $this->shortDesc;
    }

    public function setShortDesc(string $shortDesc): self
    {
        $this->shortDesc = $shortDesc;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCategoryUuid(): Uuid
    {
        return $this->categoryUuid;
    }

    public function setCategoryUuid(Uuid $uuid): self
    {
        $this->categoryUuid = $uuid;

        return $this;
    }

    public function getAuthorUuid(): Uuid
    {
        return $this->authorUuid;
    }

    public function setAuthorUuid(Uuid $uuid): self
    {
        $this->authorUuid = $uuid;

        return $this;
    }

    public function getTagUuidList(): UuidList
    {
        return $this->tagUuidList;
    }

    public function setTagUuidList(UuidList $uuidList): self
    {
        $this->tagUuidList = $uuidList;

        return $this;
    }
}

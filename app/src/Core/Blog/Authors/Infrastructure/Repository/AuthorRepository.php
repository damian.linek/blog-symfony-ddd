<?php

namespace App\Core\Blog\Authors\Infrastructure\Repository;

use App\Core\Blog\Authors\Domain\Model\Author;
use App\Core\Blog\Authors\Domain\Repository\AuthorRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\LockMode;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Author|null   find(mixed $id, LockMode|int|null $lockMode = null, int|null $lockVersion = null)
 * @method Author|null   findOneBy(array $criteria, ?array $orderBy = null)
 * @method array<Author> findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null)
 * @method array<Author> findAll()
 * @method int           count(array $criteria = [])
 */
class AuthorRepository extends ServiceEntityRepository implements AuthorRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Author::class);
    }

    public function create(Author $author): void
    {
        $this->getEntityManager()->persist($author);
        $this->commit();
    }

    public function commit(): void
    {
        $this->getEntityManager()->flush();
    }
}

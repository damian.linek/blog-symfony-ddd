<?php

namespace App\Core\Blog\Authors\Application\UpdateOne\Command;

use App\Core\Blog\Authors\Application\Data\UpdateOneDto;
use App\Core\Blog\Shared\Application\Criteria\UuidCriteria;
use App\Core\Blog\Shared\Domain\Model\Uuid;
use App\Shared\Application\Command\CommandInterface;

class UpdateOneCommand implements CommandInterface
{
    public function __construct(
        private readonly UuidCriteria $criteria,
        private readonly UpdateOneDto $dto
    ) {
    }

    public function getDto(): UpdateOneDto
    {
        return $this->dto;
    }

    public function getUuid(): Uuid
    {
        return $this->criteria->getUuid();
    }

    public function getFirstname(): string
    {
        return $this->getDto()->getFirstname();
    }

    public function getLastname(): string
    {
        return $this->getDto()->getLastname();
    }

    public function getShortDesc(): string
    {
        return $this->getDto()->getShortDesc();
    }
}

<?php

namespace App\Core\Blog\Authors\Application\UpdateOne\CommandHandler;

use App\Core\Blog\Authors\Application\UpdateOne\Command\UpdateOneCommand;
use App\Core\Blog\Authors\Domain\Factory\AuthorFactory;
use App\Core\Blog\Authors\Domain\Model\Author;
use App\Core\Blog\Authors\Domain\Repository\AuthorRepositoryInterface;
use App\Shared\Application\Command\CommandHandlerInterface;

class UpdateOneCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private readonly AuthorRepositoryInterface $repository,
        private readonly AuthorFactory $factory
    ) {
    }

    public function __invoke(UpdateOneCommand $command): void
    {
        $object = $this->repository->find($command->getUuid());

        if ($object instanceof Author) {
            $this->factory->updateOne(
                $object,
                $command->getFirstname(),
                $command->getLastname(),
                $command->getShortDesc()
            );

            $this->repository->commit();
        }
    }
}

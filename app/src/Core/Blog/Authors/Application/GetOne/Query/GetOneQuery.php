<?php

namespace App\Core\Blog\Authors\Application\GetOne\Query;

use App\Core\Blog\Shared\Application\Criteria\UuidCriteria;
use App\Shared\Application\Query\GetOne\AbstractGetOneQuery;

class GetOneQuery extends AbstractGetOneQuery
{
    public function __construct(
        UuidCriteria $uuid
    ) {
        parent::__construct($uuid);
    }
}

<?php

namespace App\Core\Blog\Authors\Application\GetOne;

use App\Core\Blog\Authors\Domain\Model\Author;

class GetDto
{
    private function __construct(
        public readonly string $uuid,
        public readonly string $firstname,
        public readonly string $lastname,
        public readonly string $shortDesc,
        public readonly \DateTimeInterface $createdAt,
        public readonly \DateTimeInterface $updatedAt,
    ) {
    }

    public static function fromEntity(Author $author): self
    {
        return new self(
            (string) $author->getUuid(),
            $author->getFirstname(),
            $author->getLastname(),
            $author->getShortDesc(),
            $author->getCreatedAt(),
            $author->getUpdatedAt(),
        );
    }
}

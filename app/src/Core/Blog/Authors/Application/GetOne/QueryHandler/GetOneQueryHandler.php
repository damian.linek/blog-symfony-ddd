<?php

namespace App\Core\Blog\Authors\Application\GetOne\QueryHandler;

use App\Core\Blog\Authors\Application\GetOne\GetDto;
use App\Core\Blog\Authors\Application\GetOne\Query\GetOneQuery;
use App\Core\Blog\Authors\Domain\Model\Author;
use App\Core\Blog\Authors\Domain\Repository\AuthorRepositoryInterface;
use App\Shared\Application\Query\QueryHandlerInterface;

class GetOneQueryHandler implements QueryHandlerInterface
{
    public function __construct(
        private readonly AuthorRepositoryInterface $repository
    ) {
    }

    public function __invoke(GetOneQuery $query): ?GetDto
    {
        $author = $this->repository->find(
            $query->getUuid()
        );

        if ($author instanceof Author) {
            return GetDto::fromEntity($author);
        }

        return null;
    }
}

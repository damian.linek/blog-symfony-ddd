<?php

namespace App\Core\Blog\Authors\Application\Data;

use App\Shared\Domain\Data\AbstractData;

class UpdateOneDto extends AbstractData
{
    private function __construct(
        private readonly string $firstname,
        private readonly string $lastname,
        private readonly string $shortDesc
    ) {
    }

    /**
     * @param array{firstname: string, lastname: string, shortDesc: string} $data
     */
    public static function fromArray(array $data): self
    {
        return new self(
            $data['firstname'],
            $data['lastname'],
            $data['shortDesc'],
        );
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function getShortDesc(): string
    {
        return $this->shortDesc;
    }
}

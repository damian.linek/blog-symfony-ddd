<?php

namespace App\Core\Blog\Authors\Application\GetList;

use App\Shared\Application\Query\GetList\GetListDtoInterface;

final readonly class GetListDto implements GetListDtoInterface
{
    /**
     * @param array<GetDto> $authors
     */
    public function __construct(
        public array $authors,
        public int $total,
    ) {
    }
}

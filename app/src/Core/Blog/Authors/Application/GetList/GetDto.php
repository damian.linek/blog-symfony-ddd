<?php

namespace App\Core\Blog\Authors\Application\GetList;

use App\Core\Blog\Authors\Domain\Model\Author;

final readonly class GetDto
{
    private function __construct(
        public string $uuid,
        public string $firstname,
        public string $lastname,
    ) {
    }

    public static function fromEntity(Author $author): self
    {
        return new self(
            $author->getUuid()->uuid(),
            $author->getFirstname(),
            $author->getLastname()
        );
    }
}

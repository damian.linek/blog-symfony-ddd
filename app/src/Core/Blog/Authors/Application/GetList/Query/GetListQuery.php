<?php

namespace App\Core\Blog\Authors\Application\GetList\Query;

use App\Core\Blog\Shared\Application\Criteria\ListCriteria;
use App\Shared\Application\Query\GetList\AbstractGetListQuery;

final class GetListQuery extends AbstractGetListQuery
{
    public function __construct(
        ListCriteria $criteria
    ) {
        parent::__construct($criteria);
    }
}

<?php

namespace App\Core\Blog\Authors\Application\GetList\Factory;

use App\Core\Blog\Authors\Application\GetList\GetDto;
use App\Core\Blog\Authors\Application\GetList\GetListDto;
use App\Core\Blog\Authors\Domain\Model\Author;
use App\Shared\Application\Query\GetList\GetListDtoFactoryInterface;
use App\Shared\Application\Query\GetList\GetListDtoInterface;

class GetListDtoFactory implements GetListDtoFactoryInterface
{
    public function create(array $objects, int $total): GetListDtoInterface
    {
        $data = [];
        foreach ($objects as $object) {
            \assert($object instanceof Author);
            $data[] = GetDto::fromEntity($object);
        }

        return new GetListDto($data, $total);
    }
}

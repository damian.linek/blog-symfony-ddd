<?php

namespace App\Core\Blog\Authors\Domain\Factory;

use App\Core\Blog\Authors\Domain\Model\Author;
use App\Core\Blog\Shared\Domain\Model\Uuid;
use App\Shared\Domain\Uuid\UuidGeneratorInterface;

class AuthorFactory
{
    public function __construct(
        private readonly UuidGeneratorInterface $uuidGenerator
    ) {
    }

    public function create(
        string $firstname,
        string $lastname,
        string $shortDesc,
    ): Author {
        return new Author(
            new Uuid($this->uuidGenerator->generate()),
            $firstname,
            $lastname,
            $shortDesc,
            new \DateTimeImmutable(),
            new \DateTimeImmutable(),
        );
    }

    public function updateOne(
        Author $author,
        string $firstname,
        string $lastname,
        string $shortDesc
    ): Author {
        return $this->update(
            $author
                ->setFirstname($firstname)
                ->setLastname($lastname)
                ->setShortDesc($shortDesc)
        );
    }

    private function update(Author $author): Author
    {
        return $author
            ->setUpdatedAt(new \DateTimeImmutable());
    }
}

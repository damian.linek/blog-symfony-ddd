<?php

namespace App\Core\Blog\Authors\Domain\Repository;

use App\Core\Blog\Authors\Domain\Model\Author;
use App\Shared\Domain\GetListRepositoryInterface;

interface AuthorRepositoryInterface extends GetListRepositoryInterface
{
    /**
     * @return ?Author
     */
    public function find(mixed $id, ?int $lockMode = null, ?int $lockVersion = null);

    /**
     * @param array<string, mixed>       $criteria
     * @param array<string, string>|null $orderBy
     *
     * @return array<Author>
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null);

    /**
     * @return array<Author>
     */
    public function findAll();

    /**
     * @param array<string, mixed>       $criteria
     * @param array<string, string>|null $orderBy
     *
     * @return array<Author>
     */
    public function findOneBy(array $criteria, ?array $orderBy = null);

    /**
     * @param array<string, mixed> $criteria
     *
     * @return int
     */
    public function count(array $criteria);

    public function create(Author $author): void;

    public function commit(): void;
}

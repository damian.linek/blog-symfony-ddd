<?php

declare(strict_types=1);

namespace App\Core\Blog\Shared\Application\Criteria;

class Sort
{
    public const DEFAULT_ORDER_BY = 'createdAt';
    public const DEFAULT_ORDER = self::ORDER_ASC;
    public const ORDER_ASC = 'ASC';
    public const ORDER_DESC = 'DESC';

    public const ORDERS = [self::ORDER_ASC, self::ORDER_DESC];

    public function __construct(
        private readonly string $field = self::DEFAULT_ORDER_BY,
        private readonly string $order = self::DEFAULT_ORDER,
    ) {
    }

    public function getField(): string
    {
        return $this->field;
    }

    public function getOrder(): string
    {
        return $this->order;
    }
}

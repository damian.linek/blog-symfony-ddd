<?php

namespace App\Core\Blog\Shared\Application\Criteria;

/**
 * @template-implements \IteratorAggregate<string, mixed>
 */
class FilterBag implements \IteratorAggregate, \Countable
{
    /**
     * @param array<string, mixed> $filters
     */
    public function __construct(
        private array $filters = []
    ) {
    }

    /**
     * @return array<string, mixed>
     */
    public function all(): array
    {
        return $this->filters;
    }

    /**
     * @return array<string>
     */
    public function keys(): array
    {
        return array_keys($this->filters);
    }

    /**
     * @param array<string, mixed> $filters
     */
    public function add(array $filters = []): void
    {
        $this->filters = array_replace($this->filters, $filters);
    }

    public function get(string $key, mixed $default = null): mixed
    {
        return \array_key_exists($key, $this->filters) ? $this->filters[$key] : $default;
    }

    public function set(string $key, mixed $value): void
    {
        $this->filters[$key] = $value;
    }

    public function has(string $key): bool
    {
        return \array_key_exists($key, $this->filters);
    }

    public function remove(string $key): void
    {
        unset($this->filters[$key]);
    }

    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->filters);
    }

    public function count(): int
    {
        return \count($this->filters);
    }
}

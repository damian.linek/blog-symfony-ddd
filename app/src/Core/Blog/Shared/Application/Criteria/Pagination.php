<?php

declare(strict_types=1);

namespace App\Core\Blog\Shared\Application\Criteria;

class Pagination
{
    public const DEFAULT_PAGE = 1;
    public const DEFAULT_LIMIT = 25;

    public function __construct(
        private readonly int $page = self::DEFAULT_PAGE,
        private readonly int $limit = self::DEFAULT_LIMIT,
    ) {
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function offset(): int
    {
        return $this->limit * ($this->page - 1);
    }
}

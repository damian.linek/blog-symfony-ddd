<?php

namespace App\Core\Blog\Shared\Application\Criteria;

use App\Core\Blog\Shared\Domain\Model\Uuid;
use App\Shared\Application\Criteria\UuidCriteriaInterface;

class UuidCriteria implements UuidCriteriaInterface
{
    public function __construct(
        private readonly Uuid $uuid
    ) {
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }
}

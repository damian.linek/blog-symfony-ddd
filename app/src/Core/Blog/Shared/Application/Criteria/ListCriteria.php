<?php

namespace App\Core\Blog\Shared\Application\Criteria;

use App\Shared\Application\Criteria\ListCriteriaInterface;

class ListCriteria implements ListCriteriaInterface
{
    public function __construct(
        private readonly FilterBag $filters,
        private readonly Sort $sort,
        private readonly Pagination $pagination
    ) {
    }

    /**
     * @return array<string, mixed>
     */
    public function getCriteria(): array
    {
        return $this->filters->all();
    }

    public function getOrderBy(): array
    {
        return [
            $this->sort->getField() => $this->sort->getOrder(),
        ];
    }

    public function getLimit(): int
    {
        return $this->pagination->getLimit();
    }

    public function getOffset(): int
    {
        return $this->pagination->offset();
    }
}

<?php

namespace App\Core\Blog\Shared\Application\Factory;

use App\Core\Blog\Shared\Application\Criteria\Pagination;
use App\Core\Blog\Shared\Application\Criteria\Sort;
use App\Shared\Application\Criteria\CriteriaFactoryInterface;
use App\Shared\Application\Exception\ConstraintViolation;
use App\Shared\Application\Exception\ConstraintViolationList;
use App\Shared\Application\Exception\ValidationException;

abstract class AbstractListCriteriaFactory implements CriteriaFactoryInterface
{
    protected ?\ReflectionClass $reflectionClass = null;

    /**
     * @param array<string, mixed> $data
     *
     * @return array{filters: array<string, mixed>, sort: array{field: string, order: string}, pagination: array{page: int, limit: int}}
     */
    protected function refactorData(array $data): array
    {
        if (!isset($data['filters'])) {
            $data['filters'] = [];
        }

        if (!isset($data['sort'])) {
            $data['sort'] = [];
        }

        \assert(\is_array($data['sort']));
        if (!isset($data['sort']['field'])) {
            $data['sort']['field'] = Sort::DEFAULT_ORDER_BY;
        }

        if (!isset($data['sort']['order'])) {
            $data['sort']['order'] = Sort::DEFAULT_ORDER;
        }

        if (!isset($data['pagination'])) {
            $data['pagination'] = [];
        }

        \assert(\is_array($data['pagination']));
        if (!isset($data['pagination']['page'])) {
            $data['pagination']['page'] = Pagination::DEFAULT_PAGE;
        }

        if (!isset($data['pagination']['limit'])) {
            $data['pagination']['limit'] = Pagination::DEFAULT_LIMIT;
        }

        \assert(\is_array($data['filters']));
        \assert(\is_string($data['sort']['field']) && \is_string($data['sort']['order']));
        \assert(\is_int($data['pagination']['page']) && \is_int($data['pagination']['limit']));

        return [
            'filters' => $data['filters'],
            'sort' => [
                'field' => $data['sort']['field'],
                'order' => $data['sort']['order'],
            ],
            'pagination' => [
                'page' => $data['pagination']['page'],
                'limit' => $data['pagination']['limit'],
            ],
        ];
    }

    /**
     * @return class-string
     */
    abstract protected function getModel(): string;

    protected function hasField(string $field): bool
    {
        if (null === $this->reflectionClass) {
            $class = $this->getModel();
            \assert(class_exists($class));
            $this->reflectionClass = new \ReflectionClass($class);
        }

        return $this->reflectionClass->hasProperty($field);
    }

    /**
     * @param array<string, mixed> $data
     */
    protected function validate(array $data): void
    {
        $errors = [];
        if (\array_key_exists('filters', $data)) {
            $errors = array_merge($errors, $this->validateFilters($data['filters']));
        }
        if (\array_key_exists('sort', $data)) {
            $errors = array_merge($errors, $this->validateSort($data['sort']));
        }
        if (\array_key_exists('pagination', $data)) {
            $errors = array_merge($errors, $this->validatePagination($data['pagination']));
        }

        if (!empty($errors)) {
            throw new ValidationException(new ConstraintViolationList($errors));
        }
    }

    /**
     * @return array<ConstraintViolation>
     */
    protected function validateFilters(mixed $data): array
    {
        if (!\is_array($data)) {
            return [new ConstraintViolation('filters', \sprintf('Invalid format of `%s`, expected %s, given %s.', 'filters', 'array', \gettype($data)))];
        }

        $errors = [];
        $keys = array_keys($data);
        foreach ($keys as $field) {
            if (!$this->hasField($field)) {
                $errors[] = new ConstraintViolation($field, \sprintf('Field `%s` is not able to be filtered.', $field));
            }
        }

        return $errors;
    }

    /**
     * @return array<ConstraintViolation>
     */
    protected function validateSort(mixed $data): array
    {
        if (!\is_array($data)) {
            return [new ConstraintViolation('sort', \sprintf('Invalid format of `%s`, expected %s, given %s.', 'sort', 'array', \gettype($data)))];
        }

        $errors = [];
        if (\array_key_exists('field', $data)) {
            if (!$this->hasField($data['field'])) {
                $errors[] = new ConstraintViolation($data['field'], \sprintf('Field `%s` is not able to be sorted by.', $data['field']));
            }
        }

        if (\array_key_exists('order', $data)) {
            if (!\in_array($data['order'], ['ASC', 'DESC'], true)) {
                $errors[] = new ConstraintViolation($data['order'], \sprintf('Sort order direction `%s` is not supported.', $data['order']));
            }
        }

        return $errors;
    }

    /**
     * @return array<ConstraintViolation>
     */
    protected function validatePagination(mixed $data): array
    {
        if (!\is_array($data)) {
            return [new ConstraintViolation('pagination', \sprintf('Invalid format of `%s`, expected %s, given %s.', 'pagination', 'array', \gettype($data)))];
        }

        $errors = [];
        if (\array_key_exists('page', $data)) {
            if (!\is_int($data['page'])) {
                $errors[] = new ConstraintViolation($data['page'], \sprintf('Pagination `%s` parameter type `%s` is not supported.', 'page', \gettype($data['page'])));
            }
        }

        if (\array_key_exists('limit', $data)) {
            if (!\is_int($data['limit'])) {
                $errors[] = new ConstraintViolation($data['page'], \sprintf('Pagination `%s` parameter type `%s` is not supported.', 'limit', \gettype($data['limit'])));
            }
        }

        return $errors;
    }
}

<?php

namespace App\Core\Blog\Shared\Application\Factory;

use App\Shared\Domain\Data\DataFactoryInterface;

abstract class AbstractDataFactory implements DataFactoryInterface
{
}

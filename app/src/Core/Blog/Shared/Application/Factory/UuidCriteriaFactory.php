<?php

namespace App\Core\Blog\Shared\Application\Factory;

use App\Core\Blog\Shared\Application\Criteria\UuidCriteria;
use App\Core\Blog\Shared\Domain\Model\Uuid;
use App\Core\Blog\Shared\Domain\Validator\ValidatorInterface;
use App\Shared\Application\Criteria\CriteriaFactoryInterface;
use App\Shared\Application\Criteria\UuidCriteriaInterface;
use App\Shared\Application\Exception\ConstraintViolation;
use App\Shared\Application\Exception\ConstraintViolationList;
use App\Shared\Application\Exception\ValidationException;

class UuidCriteriaFactory implements CriteriaFactoryInterface
{
    public function __construct(
        private readonly ValidatorInterface $uuidValidator
    ) {
    }

    /**
     * @param array<string, mixed> $data
     */
    public function create(array $data): UuidCriteriaInterface
    {
        $errors = $this->validate($data);
        if (!empty($errors)) {
            throw new ValidationException(new ConstraintViolationList($errors));
        }

        \assert(\is_string($data['uuid']));

        return new UuidCriteria(new Uuid($data['uuid']));
    }

    /**
     * @param class-string $class
     */
    public function supports(string $class): bool
    {
        return UuidCriteria::class === $class;
    }

    /**
     * @param array<string, mixed> $data
     *
     * @return array<ConstraintViolation>
     */
    private function validate(array $data): array
    {
        if (!$this->uuidValidator->validate('uuid', $data)) {
            return [new ConstraintViolation('uuid', $this->uuidValidator->getMessage())];
        }

        return [];
    }
}

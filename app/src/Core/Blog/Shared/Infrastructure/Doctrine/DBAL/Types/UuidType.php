<?php

namespace App\Core\Blog\Shared\Infrastructure\Doctrine\DBAL\Types;

use App\Core\Blog\Shared\Domain\Model\Uuid;
use App\Shared\Domain\Model\AbstractUuid;
use App\Shared\Infrastructure\Doctrine\DBAL\Types\AbstractUuidType;

class UuidType extends AbstractUuidType
{
    protected function getUuidClass(string $uuid): AbstractUuid
    {
        return new Uuid($uuid);
    }
}

<?php

namespace App\Core\Blog\Shared\Infrastructure\Doctrine\DBAL\Types;

use App\Core\Blog\Shared\Domain\Model\Uuid;
use App\Core\Blog\Shared\Domain\Model\UuidList;
use App\Shared\Domain\Model\AbstractUuidList;
use App\Shared\Infrastructure\Doctrine\DBAL\Types\AbstractUuidListType;

class UuidListType extends AbstractUuidListType
{
    protected function getUuidListClass(array $uuids): AbstractUuidList
    {
        return new UuidList(array_map(fn (string $uuid) => new Uuid($uuid), $uuids));
    }
}

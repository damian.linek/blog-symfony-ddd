<?php

namespace App\Core\Blog\Shared\Infrastructure\Validator;

use App\Core\Blog\Shared\Domain\Validator\ValidatorInterface;

class ArrayTypeValidator implements ValidatorInterface
{
    public function validate(string $fieldName, array $params): bool
    {
        if (\array_key_exists($fieldName, $params)) {
            return \is_array($params[$fieldName]);
        }

        return false;
    }

    public function getMessage(): string
    {
        return 'Property expects to be of `array` type.';
    }
}

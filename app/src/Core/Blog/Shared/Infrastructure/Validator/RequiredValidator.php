<?php

namespace App\Core\Blog\Shared\Infrastructure\Validator;

use App\Core\Blog\Shared\Domain\Validator\ValidatorInterface;

class RequiredValidator implements ValidatorInterface
{
    public function validate(string $fieldName, array $params): bool
    {
        if (\array_key_exists($fieldName, $params)) {
            return !empty($params[$fieldName]);
        }

        return false;
    }

    public function getMessage(): string
    {
        return 'Property is required.';
    }
}

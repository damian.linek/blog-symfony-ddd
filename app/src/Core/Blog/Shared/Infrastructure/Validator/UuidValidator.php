<?php

namespace App\Core\Blog\Shared\Infrastructure\Validator;

use App\Core\Blog\Shared\Domain\Validator\ValidatorInterface;
use Symfony\Component\Uid\UuidV7;

class UuidValidator implements ValidatorInterface
{
    public function validate(string $fieldName, array $params): bool
    {
        return \array_key_exists($fieldName, $params) && \is_string($params[$fieldName]) && UuidV7::isValid($params[$fieldName]);
    }

    public function getMessage(): string
    {
        return \sprintf('Property expects to be of `%s` type.', UuidV7::class);
    }
}

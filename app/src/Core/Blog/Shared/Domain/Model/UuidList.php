<?php

namespace App\Core\Blog\Shared\Domain\Model;

use App\Shared\Domain\Model\AbstractUuidList;

class UuidList extends AbstractUuidList
{
    /**
     * @param array<string> $uuids
     */
    public static function fromArray(array $uuids): self
    {
        return new self(
            array_map(fn ($uuid) => new Uuid($uuid), $uuids)
        );
    }
}

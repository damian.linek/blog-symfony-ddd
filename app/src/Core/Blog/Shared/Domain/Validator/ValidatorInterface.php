<?php

namespace App\Core\Blog\Shared\Domain\Validator;

interface ValidatorInterface
{
    /**
     * @param array<mixed> $params
     */
    public function validate(string $fieldName, array $params): bool;

    public function getMessage(): string;
}

<?php

namespace App\Core\Blog\Categories\Domain\Model;

use App\Core\Blog\Shared\Domain\Model\Uuid;

class Category
{
    public function __construct(
        private readonly Uuid $uuid,
        private string $title,
        private string $description,
        private readonly \DateTimeInterface $createdAt,
        private \DateTimeInterface $updatedAt,
    ) {
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): \DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}

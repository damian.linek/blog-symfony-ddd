<?php

namespace App\Core\Blog\Categories\Domain\Repository;

use App\Core\Blog\Categories\Domain\Model\Category;
use App\Shared\Domain\GetListRepositoryInterface;

interface CategoryRepositoryInterface extends GetListRepositoryInterface
{
    /**
     * @return ?Category
     */
    public function find(mixed $id, ?int $lockMode = null, ?int $lockVersion = null);

    /**
     * @param array<string, mixed>       $criteria
     * @param array<string, string>|null $orderBy
     *
     * @return array<Category>
     */
    public function findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null);

    /**
     * @return array<Category>
     */
    public function findAll();

    /**
     * @param array<string, mixed>       $criteria
     * @param array<string, string>|null $orderBy
     *
     * @return array<Category>
     */
    public function findOneBy(array $criteria, ?array $orderBy = null);

    /**
     * @param array<string, mixed> $criteria
     *
     * @return int
     */
    public function count(array $criteria);

    public function create(Category $category): void;

    public function commit(): void;
}

<?php

namespace App\Core\Blog\Categories\Domain\Factory;

use App\Core\Blog\Categories\Domain\Model\Category;
use App\Core\Blog\Shared\Domain\Model\Uuid;
use App\Shared\Domain\Uuid\UuidGeneratorInterface;

class CategoryFactory
{
    public function __construct(
        private readonly UuidGeneratorInterface $uuidGenerator
    ) {
    }

    public function create(
        string $title,
        string $description
    ): Category {
        return new Category(
            new Uuid($this->uuidGenerator->generate()),
            $title,
            $description,
            new \DateTimeImmutable(),
            new \DateTimeImmutable(),
        );
    }

    public function updateOne(
        Category $category,
        string $title,
        string $description,
    ): Category {
        return $this->update(
            $category
                ->setTitle($title)
                ->setDescription($description)
        );
    }

    private function update(Category $category): Category
    {
        return $category
            ->setUpdatedAt(new \DateTimeImmutable());
    }
}

<?php

namespace App\Core\Blog\Categories\Infrastructure\Repository;

use App\Core\Blog\Categories\Domain\Model\Category;
use App\Core\Blog\Categories\Domain\Repository\CategoryRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\LockMode;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Category|null   find(mixed $id, LockMode|int|null $lockMode = null, int|null $lockVersion = null)
 * @method Category|null   findOneBy(array $criteria, ?array $orderBy = null)
 * @method array<Category> findBy(array $criteria, ?array $orderBy = null, ?int $limit = null, ?int $offset = null)
 * @method array<Category> findAll()
 * @method int             count(array $criteria = [])
 */
class CategoryRepository extends ServiceEntityRepository implements CategoryRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function create(Category $category): void
    {
        $this->getEntityManager()->persist($category);
        $this->commit();
    }

    public function commit(): void
    {
        $this->getEntityManager()->flush();
    }
}

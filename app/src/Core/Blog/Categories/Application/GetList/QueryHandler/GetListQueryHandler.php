<?php

namespace App\Core\Blog\Categories\Application\GetList\QueryHandler;

use App\Core\Blog\Categories\Application\GetList\Factory\GetListDtoFactory;
use App\Core\Blog\Categories\Application\GetList\Query\GetListQuery;
use App\Core\Blog\Categories\Domain\Repository\CategoryRepositoryInterface;
use App\Shared\Application\Query\GetList\GetList;
use App\Shared\Application\Query\GetList\GetListDtoInterface;
use App\Shared\Application\Query\QueryHandlerInterface;

class GetListQueryHandler implements QueryHandlerInterface
{
    use GetList;

    public function __construct(
        protected readonly CategoryRepositoryInterface $repository,
        protected readonly GetListDtoFactory $factory,
    ) {
    }

    public function __invoke(GetListQuery $query): GetListDtoInterface
    {
        return $this->getList($query);
    }
}

<?php

namespace App\Core\Blog\Categories\Application\GetList\Factory;

use App\Core\Blog\Categories\Application\GetList\GetDto;
use App\Core\Blog\Categories\Application\GetList\GetListDto;
use App\Core\Blog\Categories\Domain\Model\Category;
use App\Shared\Application\Query\GetList\GetListDtoFactoryInterface;
use App\Shared\Application\Query\GetList\GetListDtoInterface;

class GetListDtoFactory implements GetListDtoFactoryInterface
{
    public function create(array $objects, int $total): GetListDtoInterface
    {
        $data = [];
        foreach ($objects as $object) {
            \assert($object instanceof Category);
            $data[] = GetDto::fromEntity($object);
        }

        return new GetListDto($data, $total);
    }
}

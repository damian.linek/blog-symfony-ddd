<?php

namespace App\Core\Blog\Categories\Application\GetList;

use App\Core\Blog\Categories\Domain\Model\Category;

final readonly class GetDto
{
    private function __construct(
        public string $uuid,
        public string $title,
    ) {
    }

    public static function fromEntity(Category $category): self
    {
        return new self(
            (string) $category->getUuid(),
            $category->getTitle(),
        );
    }
}

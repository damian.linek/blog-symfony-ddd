<?php

namespace App\Core\Blog\Categories\Application\GetList;

use App\Shared\Application\Query\GetList\GetListDtoInterface;

final readonly class GetListDto implements GetListDtoInterface
{
    /**
     * @param array<GetDto> $categories
     */
    public function __construct(
        public array $categories,
        public int $total,
    ) {
    }
}

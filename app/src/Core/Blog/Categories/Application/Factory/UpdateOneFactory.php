<?php

namespace App\Core\Blog\Categories\Application\Factory;

use App\Core\Blog\Categories\Application\Data\UpdateOneDto;
use App\Core\Blog\Shared\Application\Factory\AbstractDataFactory;
use App\Core\Blog\Shared\Domain\Validator\ValidatorInterface;
use App\Shared\Application\Exception\ConstraintViolation;
use App\Shared\Application\Exception\ConstraintViolationList;
use App\Shared\Application\Exception\ValidationException;

class UpdateOneFactory extends AbstractDataFactory
{
    public function __construct(
        private readonly ValidatorInterface $requiredValidator,
        private readonly ValidatorInterface $stringValidator
    ) {
    }

    /**
     * @param array<string, mixed> $data
     */
    public function create(array $data): UpdateOneDto
    {
        $this->validate($data);

        return UpdateOneDto::fromArray($this->refactorData($data));
    }

    /**
     * @param class-string $class
     */
    public function supports(string $class): bool
    {
        return UpdateOneDto::class === $class;
    }

    /**
     * @param array<mixed> $data
     *
     * @return array{title: string, description: string}
     */
    private function refactorData(array $data): array
    {
        \assert(\is_string($data['title']));
        \assert(\is_string($data['description']));

        return [
            'title' => $data['title'],
            'description' => $data['description'],
        ];
    }

    /**
     * @param array<mixed> $data
     */
    private function validate(array $data): void
    {
        $errors = $this->validateField('title', $data);
        $errors = array_merge($errors, $this->validateField('description', $data));

        if (!empty($errors)) {
            throw new ValidationException(new ConstraintViolationList($errors));
        }
    }

    /**
     * @param array<mixed> $data
     *
     * @return array<ConstraintViolation>
     */
    private function validateField(string $field, array $data): array
    {
        $errors = [];

        if (!$this->requiredValidator->validate($field, $data)) {
            $errors[] = new ConstraintViolation($field, $this->requiredValidator->getMessage());
        }

        if (!$this->stringValidator->validate($field, $data)) {
            $errors[] = new ConstraintViolation($field, $this->stringValidator->getMessage());
        }

        return $errors;
    }
}

<?php

namespace App\Core\Blog\Categories\Application\UpdateOne\CommandHandler;

use App\Core\Blog\Categories\Application\UpdateOne\Command\UpdateOneCommand;
use App\Core\Blog\Categories\Domain\Factory\CategoryFactory;
use App\Core\Blog\Categories\Domain\Model\Category;
use App\Core\Blog\Categories\Domain\Repository\CategoryRepositoryInterface;
use App\Shared\Application\Command\CommandHandlerInterface;

class UpdateOneCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private readonly CategoryRepositoryInterface $repository,
        private readonly CategoryFactory $factory
    ) {
    }

    public function __invoke(UpdateOneCommand $command): void
    {
        $object = $this->repository->find($command->getUuid());

        if ($object instanceof Category) {
            $this->factory->updateOne(
                $object,
                $command->getTitle(),
                $command->getDescription()
            );

            $this->repository->commit();
        }
    }
}

<?php

namespace App\Core\Blog\Categories\Application\GetOne\Query;

use App\Core\Blog\Shared\Domain\Model\Uuid;
use App\Shared\Application\Query\QueryInterface;

class GetOneQuery implements QueryInterface
{
    public function __construct(
        private readonly Uuid $uuid
    ) {
    }

    public function getUuid(): Uuid
    {
        return $this->uuid;
    }
}

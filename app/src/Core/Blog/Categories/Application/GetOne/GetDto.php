<?php

namespace App\Core\Blog\Categories\Application\GetOne;

use App\Core\Blog\Categories\Domain\Model\Category;

class GetDto
{
    private function __construct(
        public readonly string $uuid,
        public readonly string $title,
        public readonly string $description,
        public readonly \DateTimeInterface $createdAt,
        public readonly \DateTimeInterface $updatedAt,
    ) {
    }

    public static function fromEntity(Category $category): self
    {
        return new self(
            (string) $category->getUuid(),
            $category->getTitle(),
            $category->getDescription(),
            $category->getCreatedAt(),
            $category->getUpdatedAt(),
        );
    }
}

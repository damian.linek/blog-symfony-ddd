<?php

namespace App\Core\Blog\Categories\Application\GetOne\QueryHandler;

use App\Core\Blog\Categories\Application\GetOne\GetDto;
use App\Core\Blog\Categories\Application\GetOne\Query\GetOneQuery;
use App\Core\Blog\Categories\Domain\Model\Category;
use App\Core\Blog\Categories\Domain\Repository\CategoryRepositoryInterface;
use App\Shared\Application\Query\QueryHandlerInterface;

class GetOneQueryHandler implements QueryHandlerInterface
{
    public function __construct(
        private readonly CategoryRepositoryInterface $repository
    ) {
    }

    public function __invoke(GetOneQuery $query): ?GetDto
    {
        $category = $this->repository->find(
            $query->getUuid()
        );

        if ($category instanceof Category) {
            return GetDto::fromEntity($category);
        }

        return null;
    }
}

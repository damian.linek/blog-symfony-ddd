<?php

namespace App\Core\Blog\Categories\Application\Data;

use App\Shared\Domain\Data\AbstractData;

class UpdateOneDto extends AbstractData
{
    private function __construct(
        private readonly string $title,
        private readonly string $description
    ) {
    }

    /**
     * @param array{title: string, description: string} $data
     */
    public static function fromArray(array $data): self
    {
        return new self(
            $data['title'],
            $data['description']
        );
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}

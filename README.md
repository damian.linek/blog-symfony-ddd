# Blog Symfony Hexagonal DDD example

## Prerequisites

Before you begin, ensure you have met the following requirements:

* You have installed [Docker Engine](https://docs.docker.com/engine/) (v20.10 or higher)
* You have installed [Docker Compose](https://docs.docker.com/compose/) (v2.2 or higher)
* You have installed [GNU Make](https://www.gnu.org/software/make/)

## Structure

The project's root directory consists of two subdirectories:

* app - contains application codebase
* doc - contains application documentation
* docker - contains containers environment configuration

## Installing

To install the project, follow these steps:

```
make all
```

## Using

To use the project, follow these steps:

```
make help
```

## Further Reading

[Documentation](doc/index.md)
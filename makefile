DOCKER-COMPOSE = docker compose --env-file docker/.env --file docker/docker-compose.dev.yaml

#----------------------------------------------------------------------------------------------------------------------#
# Environment                                                                                                          #
#----------------------------------------------------------------------------------------------------------------------#

up: ## Set up containers
	@${DOCKER-COMPOSE} up --detach --build --remove-orphans

down: ## Destroy containers
	@${DOCKER-COMPOSE} down --volumes --remove-orphans

start: ## Start containers
	@${DOCKER-COMPOSE} start

stop: ## Stop containers
	@${DOCKER-COMPOSE} stop

logs: ## Show containers logs
	@${DOCKER-COMPOSE} logs --follow

shell: ## Open the app container's shell
	@${DOCKER-COMPOSE} exec -u www-data app fish

clear: ## Open the app container's shell
	@${DOCKER-COMPOSE} exec -u www-data app php bin/console cache:clear

shell-root: ## Open the app container's shell (as root user)
	@${DOCKER-COMPOSE} exec -u root app fish

lint: ## Run code quality tools
	@${DOCKER-COMPOSE} exec -u www-data app composer run lint

tests: ## Run phpunit
	@${DOCKER-COMPOSE} exec -u www-data app php bin/console --env=test doctrine:fixtures:load -q
	@${DOCKER-COMPOSE} exec -u www-data app composer run tests

#----------------------------------------------------------------------------------------------------------------------#
# Development                                                                                                          #
#----------------------------------------------------------------------------------------------------------------------#

xon: ## Enable xdebug
	@${DOCKER-COMPOSE} exec app sed -i 's/^xdebug.mode=.*/xdebug.mode=debug/' /usr/local/etc/php/conf.d/php-overrides.ini
	@${DOCKER-COMPOSE} exec app kill -USR2 1
	@echo 'Xdebug enabled'

xoff: ## Disable xdebug
	@${DOCKER-COMPOSE} exec app sed -i 's/^xdebug.mode=.*/xdebug.mode=off/' /usr/local/etc/php/conf.d/php-overrides.ini
	@${DOCKER-COMPOSE} exec app kill -USR2 1
	@echo 'Xdebug disabled'

#----------------------------------------------------------------------------------------------------------------------#
# Project                                                                                                              #
#----------------------------------------------------------------------------------------------------------------------#

all: up deps install ## Set up containers and install the project

install: ## Install the project
	make load-migrations load-fixtures
	@${DOCKER-COMPOSE} exec -u www-data app bin/console assets:install

deps: ## Install dependencies
	@${DOCKER-COMPOSE} exec -u www-data app composer install --verbose

load-fixtures: ## Install dependencies
	@${DOCKER-COMPOSE} exec -u www-data app bin/console doctrine:fixtures:load --no-interaction

load-migrations: ## Install dependencies
	@${DOCKER-COMPOSE} exec -u www-data app bin/console doctrine:migrations:migrate --no-interaction --allow-no-migration


#----------------------------------------------------------------------------------------------------------------------#
# Other                                                                                                                #
#----------------------------------------------------------------------------------------------------------------------#

help: ## Display available targets
	@awk \
		'BEGIN {FS = ":.*##"; printf "\n\033[1mUsage:\033[0m\n  make \033[32m<target>\033[0m\n"; } \
		END { printf "\n"; } \
		/^#[^#-]/ { printf "\n \033[1m%s\033[0m\n", substr($$0, 2, length($$0)-6); } \
		/^[a-zA-Z@_-]+:.*?##/ { printf "    \033[32m%-15s\033[0m %s\n", $$1, $$2; }' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help
